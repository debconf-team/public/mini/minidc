#!/bin/sh

set -eu

sitename=$1
localsettings=$(readlink -f localsettings.py)
outputdir=${APP_DATA_DIR:-.}
tmpdir=$(mktemp -d)
set -x
if ! grep -q WAFER_HIDE_LOGIN $localsettings; then
  echo "WAFER_HIDE_LOGIN = True" >> $localsettings
fi
./manage.py build --skip-static --build-dir $tmpdir/$sitename
cp -r localstatic $tmpdir/$sitename/static
cp localstatic/img/favicon.ico $tmpdir/$sitename/
tar caf $outputdir/$sitename.tar.gz -C $tmpdir $sitename
rm -rf $tmpdir
