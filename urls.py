import re
from django.conf.urls import include
from django.urls import re_path
from django.views.static import serve
from django.conf import settings

urlpatterns = [
    re_path(r"", include("minidebconf.urls")),
    re_path(
        r"^%s(?P<path>.*)$" % re.escape(settings.MEDIA_URL.lstrip("/")),
        serve,
        {"document_root": settings.MEDIA_ROOT},
    ),
]
