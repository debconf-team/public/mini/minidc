FROM debian:bookworm-backports AS base
RUN apt-get update && \
    apt-get install -qy auto-apt-proxy && \
    apt-get install -qy \
        ca-certificates \
        fonts-fork-awesome \
        gettext \
        gunicorn \
        libjs-bootstrap4 \
        libjs-eonasdan-bootstrap-datetimepicker \
        libjs-jquery \
        libjs-moment \
        libjs-moment-timezone \
        node-normalize.css \
        python3 \
        python3-bleach \
        python3-boto3 \
        python3-botocore \
        python3-diff-match-patch \
        python3-django \
        python3-django-compressor \
        python3-django-countries \
        python3-django-crispy-forms \
        python3-django-extensions \
        python3-django-libsass \
        python3-djangorestframework \
        python3-djangorestframework-extensions \
        python3-django-reversion \
        python3-fs \
        python3-icalendar \
        python3-pil \
        python3-pip \
        python3-pkg-resources \
        python3-pytest \
        python3-pytest-django \
        python3-pytest-mock \
        python3-s3transfer \
        python3-setuptools \
        python3-six \
        python3-whitenoise \
        python3-yaml \
    && echo "Base packages installed"

FROM base AS build
RUN apt-get update  -q && \
    apt-get install -qy \
        git \
        python3-pip \
        rsync \
    && \
    echo "Build packages installed"

# stuff from pypi
COPY requirements-base.txt ./requirements-base.txt
RUN pip3 install --break-system-packages -r requirements-base.txt
COPY requirements.txt ./requirements.txt
RUN pip3 install --break-system-packages -r requirements.txt && \
    pip3 list --local && \
    rm -rf /root/.cache

FROM base
WORKDIR /app
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/bin /usr/local/bin
COPY . ./

ENV LANG=C.UTF-8
ENV DATADIR=/data
RUN ln -sfT /data/localsettings.py localsettings.py
RUN ./manage.py collectstatic --no-input -v 0 && ./manage.py compress --force

USER nobody
CMD if [ ! -f /data/localsettings.py ]; then cp localsettings.py.example /data/localsettings.py; fi && \
    ./manage.py migrate && \
    ./manage.py createcachetable && \
    ./manage.py wafer_add_default_groups && \
    ./manage.py create_debconf_groups && \
    ./manage.py init_minidc_menu_pages && \
    gunicorn --workers=2 --threads=4 --worker-class=gthread wsgi
